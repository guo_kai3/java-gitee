
# ContentBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**size** | **String** |  |  [optional]
**sha** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**downloadUrl** | **String** |  |  [optional]
**links** | **String** |  |  [optional]



