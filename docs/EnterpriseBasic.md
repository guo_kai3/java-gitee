
# EnterpriseBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**path** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]



