
# EnterpriseMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  |  [optional]
**active** | **String** |  |  [optional]
**remark** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**outsourced** | **String** |  |  [optional]
**enterprise** | [**EnterpriseBasic**](EnterpriseBasic.md) |  |  [optional]
**user** | **String** |  |  [optional]



