
# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**type** | **String** |  |  [optional]
**actor** | **String** |  |  [optional]
**repo** | **String** |  |  [optional]
**org** | **String** |  |  [optional]
**_public** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**payload** | **Object** | 不同类型动态的内容 |  [optional]



