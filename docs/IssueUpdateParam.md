
# IssueUpdateParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**repo** | **String** | 仓库路径(path) |  [optional]
**title** | **String** | Issue标题 |  [optional]
**state** | [**StateEnum**](#StateEnum) | Issue 状态，open（开启的）、progressing（进行中）、closed（关闭的） |  [optional]
**body** | **String** | Issue描述 |  [optional]
**assignee** | **String** | Issue负责人的username |  [optional]
**milestone** | **Integer** | 里程碑序号 |  [optional]
**labels** | **String** | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance |  [optional]
**program** | **String** | 项目ID |  [optional]


<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
OPEN | &quot;open&quot;
PROGRESSING | &quot;progressing&quot;
CLOSED | &quot;closed&quot;



