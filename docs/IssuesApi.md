# IssuesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#deleteV5ReposOwnerRepoIssuesCommentsId) | **DELETE** /v5/repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
[**getV5EnterprisesEnterpriseIssues**](IssuesApi.md#getV5EnterprisesEnterpriseIssues) | **GET** /v5/enterprises/{enterprise}/issues | 获取某个企业的所有Issues
[**getV5EnterprisesEnterpriseIssuesNumber**](IssuesApi.md#getV5EnterprisesEnterpriseIssuesNumber) | **GET** /v5/enterprises/{enterprise}/issues/{number} | 获取企业的某个Issue
[**getV5EnterprisesEnterpriseIssuesNumberComments**](IssuesApi.md#getV5EnterprisesEnterpriseIssuesNumberComments) | **GET** /v5/enterprises/{enterprise}/issues/{number}/comments | 获取企业某个Issue所有评论
[**getV5EnterprisesEnterpriseIssuesNumberLabels**](IssuesApi.md#getV5EnterprisesEnterpriseIssuesNumberLabels) | **GET** /v5/enterprises/{enterprise}/issues/{number}/labels | 获取企业某个Issue所有标签
[**getV5Issues**](IssuesApi.md#getV5Issues) | **GET** /v5/issues | 获取当前授权用户的所有Issues
[**getV5OrgsOrgIssues**](IssuesApi.md#getV5OrgsOrgIssues) | **GET** /v5/orgs/{org}/issues | 获取当前用户某个组织的Issues
[**getV5ReposOwnerIssuesNumberOperateLogs**](IssuesApi.md#getV5ReposOwnerIssuesNumberOperateLogs) | **GET** /v5/repos/{owner}/issues/{number}/operate_logs | 获取某个Issue下的操作日志
[**getV5ReposOwnerRepoIssues**](IssuesApi.md#getV5ReposOwnerRepoIssues) | **GET** /v5/repos/{owner}/{repo}/issues | 仓库的所有Issues
[**getV5ReposOwnerRepoIssuesComments**](IssuesApi.md#getV5ReposOwnerRepoIssuesComments) | **GET** /v5/repos/{owner}/{repo}/issues/comments | 获取仓库所有Issue的评论
[**getV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#getV5ReposOwnerRepoIssuesCommentsId) | **GET** /v5/repos/{owner}/{repo}/issues/comments/{id} | 获取仓库Issue某条评论
[**getV5ReposOwnerRepoIssuesNumber**](IssuesApi.md#getV5ReposOwnerRepoIssuesNumber) | **GET** /v5/repos/{owner}/{repo}/issues/{number} | 仓库的某个Issue
[**getV5ReposOwnerRepoIssuesNumberComments**](IssuesApi.md#getV5ReposOwnerRepoIssuesNumberComments) | **GET** /v5/repos/{owner}/{repo}/issues/{number}/comments | 获取仓库某个Issue所有的评论
[**getV5UserIssues**](IssuesApi.md#getV5UserIssues) | **GET** /v5/user/issues | 获取授权用户的所有Issues
[**patchV5ReposOwnerIssuesNumber**](IssuesApi.md#patchV5ReposOwnerIssuesNumber) | **PATCH** /v5/repos/{owner}/issues/{number} | 更新Issue
[**patchV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#patchV5ReposOwnerRepoIssuesCommentsId) | **PATCH** /v5/repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
[**postV5ReposOwnerIssues**](IssuesApi.md#postV5ReposOwnerIssues) | **POST** /v5/repos/{owner}/issues | 创建Issue
[**postV5ReposOwnerRepoIssuesNumberComments**](IssuesApi.md#postV5ReposOwnerRepoIssuesNumberComments) | **POST** /v5/repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论


<a name="deleteV5ReposOwnerRepoIssuesCommentsId"></a>
# **deleteV5ReposOwnerRepoIssuesCommentsId**
> deleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken)

删除Issue某条评论

删除Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#deleteV5ReposOwnerRepoIssuesCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseIssues"></a>
# **getV5EnterprisesEnterpriseIssues**
> List&lt;Issue&gt; getV5EnterprisesEnterpriseIssues(enterprise, accessToken, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program)

获取某个企业的所有Issues

获取某个企业的所有Issues

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
String milestone = "milestone_example"; // String | 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
String assignee = "assignee_example"; // String | 用户的username。 none为没指派者, *为所有带有指派者的
String creator = "creator_example"; // String | 创建Issues的用户username
String program = "program_example"; // String | 所属项目名称。none为没所属有项目的，*为所有带所属项目的
try {
    List<Issue> result = apiInstance.getV5EnterprisesEnterpriseIssues(enterprise, accessToken, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5EnterprisesEnterpriseIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]
 **milestone** | **String**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | [optional]
 **assignee** | **String**| 用户的username。 none为没指派者, *为所有带有指派者的 | [optional]
 **creator** | **String**| 创建Issues的用户username | [optional]
 **program** | **String**| 所属项目名称。none为没所属有项目的，*为所有带所属项目的 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseIssuesNumber"></a>
# **getV5EnterprisesEnterpriseIssuesNumber**
> Issue getV5EnterprisesEnterpriseIssuesNumber(enterprise, number, accessToken)

获取企业的某个Issue

获取企业的某个Issue

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Issue result = apiInstance.getV5EnterprisesEnterpriseIssuesNumber(enterprise, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5EnterprisesEnterpriseIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseIssuesNumberComments"></a>
# **getV5EnterprisesEnterpriseIssuesNumberComments**
> List&lt;Note&gt; getV5EnterprisesEnterpriseIssuesNumberComments(enterprise, number, accessToken, page, perPage)

获取企业某个Issue所有评论

获取企业某个Issue所有评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Note> result = apiInstance.getV5EnterprisesEnterpriseIssuesNumberComments(enterprise, number, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5EnterprisesEnterpriseIssuesNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Note&gt;**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5EnterprisesEnterpriseIssuesNumberLabels"></a>
# **getV5EnterprisesEnterpriseIssuesNumberLabels**
> List&lt;Label&gt; getV5EnterprisesEnterpriseIssuesNumberLabels(enterprise, number, accessToken, page, perPage)

获取企业某个Issue所有标签

获取企业某个Issue所有标签

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String enterprise = "enterprise_example"; // String | 企业的路径(path/login)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Label> result = apiInstance.getV5EnterprisesEnterpriseIssuesNumberLabels(enterprise, number, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5EnterprisesEnterpriseIssuesNumberLabels");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **enterprise** | **String**| 企业的路径(path/login) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Label&gt;**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5Issues"></a>
# **getV5Issues**
> List&lt;Issue&gt; getV5Issues(accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt)

获取当前授权用户的所有Issues

获取当前授权用户的所有Issues

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
try {
    List<Issue> result = apiInstance.getV5Issues(accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5Issues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5OrgsOrgIssues"></a>
# **getV5OrgsOrgIssues**
> List&lt;Issue&gt; getV5OrgsOrgIssues(org, accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt)

获取当前用户某个组织的Issues

获取当前用户某个组织的Issues

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String org = "org_example"; // String | 组织的路径(path/login)
String accessToken = "accessToken_example"; // String | 用户授权码
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
try {
    List<Issue> result = apiInstance.getV5OrgsOrgIssues(org, accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5OrgsOrgIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **String**| 组织的路径(path/login) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerIssuesNumberOperateLogs"></a>
# **getV5ReposOwnerIssuesNumberOperateLogs**
> List&lt;OperateLog&gt; getV5ReposOwnerIssuesNumberOperateLogs(owner, number, accessToken, repo, sort)

获取某个Issue下的操作日志

获取某个Issue下的操作日志

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
String repo = "repo_example"; // String | 仓库路径(path)
String sort = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    List<OperateLog> result = apiInstance.getV5ReposOwnerIssuesNumberOperateLogs(owner, number, accessToken, repo, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5ReposOwnerIssuesNumberOperateLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **repo** | **String**| 仓库路径(path) | [optional]
 **sort** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: desc, asc]

### Return type

[**List&lt;OperateLog&gt;**](OperateLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssues"></a>
# **getV5ReposOwnerRepoIssues**
> List&lt;Issue&gt; getV5ReposOwnerRepoIssues(owner, repo, accessToken, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program)

仓库的所有Issues

仓库的所有Issues

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
String milestone = "milestone_example"; // String | 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
String assignee = "assignee_example"; // String | 用户的username。 none为没指派者, *为所有带有指派者的
String creator = "creator_example"; // String | 创建Issues的用户username
String program = "program_example"; // String | 所属项目名称。none为没有所属项目，*为所有带所属项目的
try {
    List<Issue> result = apiInstance.getV5ReposOwnerRepoIssues(owner, repo, accessToken, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5ReposOwnerRepoIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]
 **milestone** | **String**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | [optional]
 **assignee** | **String**| 用户的username。 none为没指派者, *为所有带有指派者的 | [optional]
 **creator** | **String**| 创建Issues的用户username | [optional]
 **program** | **String**| 所属项目名称。none为没有所属项目，*为所有带所属项目的 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesComments"></a>
# **getV5ReposOwnerRepoIssuesComments**
> Note getV5ReposOwnerRepoIssuesComments(owner, repo, accessToken, sort, direction, since, page, perPage)

获取仓库所有Issue的评论

获取仓库所有Issue的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | Either created or updated. Default: created
String direction = "asc"; // String | Either asc or desc. Ignored without the sort parameter.
String since = "since_example"; // String | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Note result = apiInstance.getV5ReposOwnerRepoIssuesComments(owner, repo, accessToken, sort, direction, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5ReposOwnerRepoIssuesComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| Either created or updated. Default: created | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| Either asc or desc. Ignored without the sort parameter. | [optional] [default to asc] [enum: asc, desc]
 **since** | **String**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesCommentsId"></a>
# **getV5ReposOwnerRepoIssuesCommentsId**
> Note getV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken)

获取仓库Issue某条评论

获取仓库Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Note result = apiInstance.getV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5ReposOwnerRepoIssuesCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesNumber"></a>
# **getV5ReposOwnerRepoIssuesNumber**
> Issue getV5ReposOwnerRepoIssuesNumber(owner, repo, number, accessToken)

仓库的某个Issue

仓库的某个Issue

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Issue result = apiInstance.getV5ReposOwnerRepoIssuesNumber(owner, repo, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5ReposOwnerRepoIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoIssuesNumberComments"></a>
# **getV5ReposOwnerRepoIssuesNumberComments**
> Note getV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, accessToken, since, page, perPage)

获取仓库某个Issue所有的评论

获取仓库某个Issue所有的评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
String accessToken = "accessToken_example"; // String | 用户授权码
String since = "since_example"; // String | Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    Note result = apiInstance.getV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, accessToken, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5ReposOwnerRepoIssuesNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **since** | **String**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5UserIssues"></a>
# **getV5UserIssues**
> List&lt;Issue&gt; getV5UserIssues(accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt)

获取授权用户的所有Issues

获取授权用户的所有Issues

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String accessToken = "accessToken_example"; // String | 用户授权码
String filter = "assigned"; // String | 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
String state = "open"; // String | Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
String sort = "created"; // String | 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
String direction = "desc"; // String | 排序方式: 升序(asc)，降序(desc)。默认: desc
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
String schedule = "schedule_example"; // String | 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
String deadline = "deadline_example"; // String | 计划截止日期，格式同上
String createdAt = "createdAt_example"; // String | 任务创建时间，格式同上
String finishedAt = "finishedAt_example"; // String | 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
try {
    List<Issue> result = apiInstance.getV5UserIssues(accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#getV5UserIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **String**| 用户授权码 | [optional]
 **filter** | **String**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [optional] [default to assigned] [enum: assigned, created, all]
 **state** | **String**| Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open | [optional] [default to open] [enum: open, progressing, closed, rejected, all]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **sort** | **String**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]
 **schedule** | **String**| 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80 | [optional]
 **deadline** | **String**| 计划截止日期，格式同上 | [optional]
 **createdAt** | **String**| 任务创建时间，格式同上 | [optional]
 **finishedAt** | **String**| 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上 | [optional]

### Return type

[**List&lt;Issue&gt;**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerIssuesNumber"></a>
# **patchV5ReposOwnerIssuesNumber**
> Issue patchV5ReposOwnerIssuesNumber(owner, number, body)

更新Issue

更新Issue

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
IssueUpdateParam body = new IssueUpdateParam(); // IssueUpdateParam | 可选。Issue 内容
try {
    Issue result = apiInstance.patchV5ReposOwnerIssuesNumber(owner, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#patchV5ReposOwnerIssuesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**IssueUpdateParam**](IssueUpdateParam.md)| 可选。Issue 内容 |

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoIssuesCommentsId"></a>
# **patchV5ReposOwnerRepoIssuesCommentsId**
> Note patchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, accessToken)

更新Issue某条评论

更新Issue某条评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | The contents of the comment.
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Note result = apiInstance.patchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#patchV5ReposOwnerRepoIssuesCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| The contents of the comment. |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerIssues"></a>
# **postV5ReposOwnerIssues**
> Issue postV5ReposOwnerIssues(owner, title, accessToken, repo, issueType, body, assignee, milestone, labels, program)

创建Issue

创建Issue

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String title = "title_example"; // String | Issue标题
String accessToken = "accessToken_example"; // String | 用户授权码
String repo = "repo_example"; // String | 仓库路径(path)
String issueType = "issueType_example"; // String | 企业自定义任务类型，非企业默认任务类型为“任务”
String body = "body_example"; // String | Issue描述
String assignee = "assignee_example"; // String | Issue负责人的username
Integer milestone = 56; // Integer | 里程碑序号
String labels = "labels_example"; // String | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
String program = "program_example"; // String | 项目ID
try {
    Issue result = apiInstance.postV5ReposOwnerIssues(owner, title, accessToken, repo, issueType, body, assignee, milestone, labels, program);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#postV5ReposOwnerIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **title** | **String**| Issue标题 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **repo** | **String**| 仓库路径(path) | [optional]
 **issueType** | **String**| 企业自定义任务类型，非企业默认任务类型为“任务” | [optional]
 **body** | **String**| Issue描述 | [optional]
 **assignee** | **String**| Issue负责人的username | [optional]
 **milestone** | **Integer**| 里程碑序号 | [optional]
 **labels** | **String**| 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance | [optional]
 **program** | **String**| 项目ID | [optional]

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoIssuesNumberComments"></a>
# **postV5ReposOwnerRepoIssuesNumberComments**
> Note postV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body)

创建某个Issue评论

创建某个Issue评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.IssuesApi;


IssuesApi apiInstance = new IssuesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String number = "number_example"; // String | Issue 编号(区分大小写，无需添加 # 号)
IssueCommentPostParam body = new IssueCommentPostParam(); // IssueCommentPostParam | Issue comment内容
try {
    Note result = apiInstance.postV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling IssuesApi#postV5ReposOwnerRepoIssuesNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **String**| Issue 编号(区分大小写，无需添加 # 号) |
 **body** | [**IssueCommentPostParam**](IssueCommentPostParam.md)| Issue comment内容 |

### Return type

[**Note**](Note.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

