# MilestonesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#deleteV5ReposOwnerRepoMilestonesNumber) | **DELETE** /v5/repos/{owner}/{repo}/milestones/{number} | 删除仓库单个里程碑
[**getV5ReposOwnerRepoMilestones**](MilestonesApi.md#getV5ReposOwnerRepoMilestones) | **GET** /v5/repos/{owner}/{repo}/milestones | 获取仓库所有里程碑
[**getV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#getV5ReposOwnerRepoMilestonesNumber) | **GET** /v5/repos/{owner}/{repo}/milestones/{number} | 获取仓库单个里程碑
[**patchV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#patchV5ReposOwnerRepoMilestonesNumber) | **PATCH** /v5/repos/{owner}/{repo}/milestones/{number} | 更新仓库里程碑
[**postV5ReposOwnerRepoMilestones**](MilestonesApi.md#postV5ReposOwnerRepoMilestones) | **POST** /v5/repos/{owner}/{repo}/milestones | 创建仓库里程碑


<a name="deleteV5ReposOwnerRepoMilestonesNumber"></a>
# **deleteV5ReposOwnerRepoMilestonesNumber**
> deleteV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken)

删除仓库单个里程碑

删除仓库单个里程碑

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MilestonesApi;


MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 里程碑序号(id)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#deleteV5ReposOwnerRepoMilestonesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 里程碑序号(id) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoMilestones"></a>
# **getV5ReposOwnerRepoMilestones**
> List&lt;Milestone&gt; getV5ReposOwnerRepoMilestones(owner, repo, accessToken, state, sort, direction, page, perPage)

获取仓库所有里程碑

获取仓库所有里程碑

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MilestonesApi;


MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String sort = "due_on"; // String | 排序方式: due_on
String direction = "direction_example"; // String | 升序(asc)或是降序(desc)。默认: asc
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<Milestone> result = apiInstance.getV5ReposOwnerRepoMilestones(owner, repo, accessToken, state, sort, direction, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#getV5ReposOwnerRepoMilestones");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **sort** | **String**| 排序方式: due_on | [optional] [default to due_on] [enum: due_on]
 **direction** | **String**| 升序(asc)或是降序(desc)。默认: asc | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;Milestone&gt;**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoMilestonesNumber"></a>
# **getV5ReposOwnerRepoMilestonesNumber**
> Milestone getV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken)

获取仓库单个里程碑

获取仓库单个里程碑

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MilestonesApi;


MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 里程碑序号(id)
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    Milestone result = apiInstance.getV5ReposOwnerRepoMilestonesNumber(owner, repo, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#getV5ReposOwnerRepoMilestonesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 里程碑序号(id) |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoMilestonesNumber"></a>
# **patchV5ReposOwnerRepoMilestonesNumber**
> Milestone patchV5ReposOwnerRepoMilestonesNumber(owner, repo, number, title, dueOn, accessToken, state, description)

更新仓库里程碑

更新仓库里程碑

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MilestonesApi;


MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 里程碑序号(id)
String title = "title_example"; // String | 里程碑标题
String dueOn = "dueOn_example"; // String | 里程碑的截止日期
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String description = "description_example"; // String | 里程碑具体描述
try {
    Milestone result = apiInstance.patchV5ReposOwnerRepoMilestonesNumber(owner, repo, number, title, dueOn, accessToken, state, description);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#patchV5ReposOwnerRepoMilestonesNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 里程碑序号(id) |
 **title** | **String**| 里程碑标题 |
 **dueOn** | **String**| 里程碑的截止日期 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **description** | **String**| 里程碑具体描述 | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoMilestones"></a>
# **postV5ReposOwnerRepoMilestones**
> Milestone postV5ReposOwnerRepoMilestones(owner, repo, title, dueOn, accessToken, state, description)

创建仓库里程碑

创建仓库里程碑

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.MilestonesApi;


MilestonesApi apiInstance = new MilestonesApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String title = "title_example"; // String | 里程碑标题
String dueOn = "dueOn_example"; // String | 里程碑的截止日期
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 里程碑状态: open, closed, all。默认: open
String description = "description_example"; // String | 里程碑具体描述
try {
    Milestone result = apiInstance.postV5ReposOwnerRepoMilestones(owner, repo, title, dueOn, accessToken, state, description);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MilestonesApi#postV5ReposOwnerRepoMilestones");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **title** | **String**| 里程碑标题 |
 **dueOn** | **String**| 里程碑的截止日期 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 里程碑状态: open, closed, all。默认: open | [optional] [default to open] [enum: open, closed, all]
 **description** | **String**| 里程碑具体描述 | [optional]

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

