
# Namespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**type** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**path** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**parent** | [**NamespaceMini**](NamespaceMini.md) |  |  [optional]



