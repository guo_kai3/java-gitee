
# Note

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**body** | **String** |  |  [optional]
**bodyHtml** | **String** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**source** | **String** |  |  [optional]
**createdAt** | **String** |  |  [optional]
**target** | **String** |  |  [optional]



