
# ProgramBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**assignee** | **String** |  |  [optional]
**author** | **String** |  |  [optional]



