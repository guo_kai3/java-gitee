
# PullRequestCommentPostParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** | 用户授权码 |  [optional]
**body** | **String** | 必填。评论内容 |  [optional]
**commitId** | **String** | 可选。PR代码评论的commit id |  [optional]
**path** | **String** | 可选。PR代码评论的文件名 |  [optional]
**position** | **Integer** | 可选。PR代码评论diff中的行数 |  [optional]



