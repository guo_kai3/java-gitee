# PullRequestsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#deleteV5ReposOwnerRepoPullsCommentsId) | **DELETE** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
[**deleteV5ReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#deleteV5ReposOwnerRepoPullsNumberAssignees) | **DELETE** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 取消用户审查 Pull Request
[**deleteV5ReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#deleteV5ReposOwnerRepoPullsNumberTesters) | **DELETE** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 取消用户测试 Pull Request
[**getV5ReposOwnerRepoPulls**](PullRequestsApi.md#getV5ReposOwnerRepoPulls) | **GET** /v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
[**getV5ReposOwnerRepoPullsComments**](PullRequestsApi.md#getV5ReposOwnerRepoPullsComments) | **GET** /v5/repos/{owner}/{repo}/pulls/comments | 获取该仓库下的所有Pull Request评论
[**getV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#getV5ReposOwnerRepoPullsCommentsId) | **GET** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
[**getV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumber) | **GET** /v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
[**getV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberComments) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
[**getV5ReposOwnerRepoPullsNumberCommits**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberCommits) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
[**getV5ReposOwnerRepoPullsNumberFiles**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberFiles) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
[**getV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberMerge) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
[**getV5ReposOwnerRepoPullsNumberOperateLogs**](PullRequestsApi.md#getV5ReposOwnerRepoPullsNumberOperateLogs) | **GET** /v5/repos/{owner}/{repo}/pulls/{number}/operate_logs | 获取某个Pull Request的操作日志
[**patchV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#patchV5ReposOwnerRepoPullsCommentsId) | **PATCH** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
[**patchV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#patchV5ReposOwnerRepoPullsNumber) | **PATCH** /v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
[**postV5ReposOwnerRepoPulls**](PullRequestsApi.md#postV5ReposOwnerRepoPulls) | **POST** /v5/repos/{owner}/{repo}/pulls | 创建Pull Request
[**postV5ReposOwnerRepoPullsNumberAssignees**](PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberAssignees) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/assignees | 指派用户审查 Pull Request
[**postV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberComments) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
[**postV5ReposOwnerRepoPullsNumberTesters**](PullRequestsApi.md#postV5ReposOwnerRepoPullsNumberTesters) | **POST** /v5/repos/{owner}/{repo}/pulls/{number}/testers | 指派用户测试 Pull Request
[**putV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#putV5ReposOwnerRepoPullsNumberMerge) | **PUT** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request


<a name="deleteV5ReposOwnerRepoPullsCommentsId"></a>
# **deleteV5ReposOwnerRepoPullsCommentsId**
> deleteV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken)

删除评论

删除评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.deleteV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteV5ReposOwnerRepoPullsCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoPullsNumberAssignees"></a>
# **deleteV5ReposOwnerRepoPullsNumberAssignees**
> PullRequest deleteV5ReposOwnerRepoPullsNumberAssignees(owner, repo, number, assignees, accessToken)

取消用户审查 Pull Request

取消用户审查 Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String assignees = "assignees_example"; // String | 用户的个人空间地址, 以 , 分隔
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequest result = apiInstance.deleteV5ReposOwnerRepoPullsNumberAssignees(owner, repo, number, assignees, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteV5ReposOwnerRepoPullsNumberAssignees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **assignees** | **String**| 用户的个人空间地址, 以 , 分隔 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="deleteV5ReposOwnerRepoPullsNumberTesters"></a>
# **deleteV5ReposOwnerRepoPullsNumberTesters**
> PullRequest deleteV5ReposOwnerRepoPullsNumberTesters(owner, repo, number, testers, accessToken)

取消用户测试 Pull Request

取消用户测试 Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String testers = "testers_example"; // String | 用户的个人空间地址, 以 , 分隔
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequest result = apiInstance.deleteV5ReposOwnerRepoPullsNumberTesters(owner, repo, number, testers, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#deleteV5ReposOwnerRepoPullsNumberTesters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **testers** | **String**| 用户的个人空间地址, 以 , 分隔 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPulls"></a>
# **getV5ReposOwnerRepoPulls**
> List&lt;PullRequest&gt; getV5ReposOwnerRepoPulls(owner, repo, accessToken, state, head, base, sort, direction, milestoneNumber, labels, page, perPage)

获取Pull Request列表

获取Pull Request列表

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String state = "open"; // String | 可选。Pull Request 状态
String head = "head_example"; // String | 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
String base = "base_example"; // String | 可选。Pull Request 提交目标分支的名称。
String sort = "created"; // String | 可选。排序字段，默认按创建时间
String direction = "desc"; // String | 可选。升序/降序
Integer milestoneNumber = 56; // Integer | 可选。里程碑序号(id)
String labels = "labels_example"; // String | 用逗号分开的标签。如: bug,performance
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<PullRequest> result = apiInstance.getV5ReposOwnerRepoPulls(owner, repo, accessToken, state, head, base, sort, direction, milestoneNumber, labels, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPulls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **state** | **String**| 可选。Pull Request 状态 | [optional] [default to open] [enum: open, closed, merged, all]
 **head** | **String**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | [optional]
 **base** | **String**| 可选。Pull Request 提交目标分支的名称。 | [optional]
 **sort** | **String**| 可选。排序字段，默认按创建时间 | [optional] [default to created] [enum: created, updated, popularity, long-running]
 **direction** | **String**| 可选。升序/降序 | [optional] [default to desc] [enum: asc, desc]
 **milestoneNumber** | **Integer**| 可选。里程碑序号(id) | [optional]
 **labels** | **String**| 用逗号分开的标签。如: bug,performance | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;PullRequest&gt;**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsComments"></a>
# **getV5ReposOwnerRepoPullsComments**
> List&lt;PullRequestComments&gt; getV5ReposOwnerRepoPullsComments(owner, repo, accessToken, sort, direction, since, page, perPage)

获取该仓库下的所有Pull Request评论

获取该仓库下的所有Pull Request评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "created"; // String | 可选。按创建时间/更新时间排序
String direction = "desc"; // String | 可选。升序/降序
String since = "since_example"; // String | 起始的更新时间，要求时间格式为 ISO 8601
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<PullRequestComments> result = apiInstance.getV5ReposOwnerRepoPullsComments(owner, repo, accessToken, sort, direction, since, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 可选。按创建时间/更新时间排序 | [optional] [default to created] [enum: created, updated]
 **direction** | **String**| 可选。升序/降序 | [optional] [default to desc] [enum: asc, desc]
 **since** | **String**| 起始的更新时间，要求时间格式为 ISO 8601 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;PullRequestComments&gt;**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsCommentsId"></a>
# **getV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments getV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken)

获取Pull Request的某个评论

获取Pull Request的某个评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequestComments result = apiInstance.getV5ReposOwnerRepoPullsCommentsId(owner, repo, id, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**|  |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumber"></a>
# **getV5ReposOwnerRepoPullsNumber**
> PullRequest getV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken)

获取单个Pull Request

获取单个Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequest result = apiInstance.getV5ReposOwnerRepoPullsNumber(owner, repo, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberComments"></a>
# **getV5ReposOwnerRepoPullsNumberComments**
> List&lt;PullRequestComments&gt; getV5ReposOwnerRepoPullsNumberComments(owner, repo, number, accessToken, page, perPage)

获取某个Pull Request的所有评论

获取某个Pull Request的所有评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
Integer page = 1; // Integer | 当前的页码
Integer perPage = 20; // Integer | 每页的数量，最大为 100
try {
    List<PullRequestComments> result = apiInstance.getV5ReposOwnerRepoPullsNumberComments(owner, repo, number, accessToken, page, perPage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **page** | **Integer**| 当前的页码 | [optional] [default to 1]
 **perPage** | **Integer**| 每页的数量，最大为 100 | [optional] [default to 20]

### Return type

[**List&lt;PullRequestComments&gt;**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberCommits"></a>
# **getV5ReposOwnerRepoPullsNumberCommits**
> List&lt;PullRequestCommits&gt; getV5ReposOwnerRepoPullsNumberCommits(owner, repo, number, accessToken)

获取某Pull Request的所有Commit信息。最多显示250条Commit

获取某Pull Request的所有Commit信息。最多显示250条Commit

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<PullRequestCommits> result = apiInstance.getV5ReposOwnerRepoPullsNumberCommits(owner, repo, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsNumberCommits");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;PullRequestCommits&gt;**](PullRequestCommits.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberFiles"></a>
# **getV5ReposOwnerRepoPullsNumberFiles**
> List&lt;PullRequestFiles&gt; getV5ReposOwnerRepoPullsNumberFiles(owner, repo, number, accessToken)

Pull Request Commit文件列表。最多显示300条diff

Pull Request Commit文件列表。最多显示300条diff

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    List<PullRequestFiles> result = apiInstance.getV5ReposOwnerRepoPullsNumberFiles(owner, repo, number, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsNumberFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**List&lt;PullRequestFiles&gt;**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberMerge"></a>
# **getV5ReposOwnerRepoPullsNumberMerge**
> getV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken)

判断Pull Request是否已经合并

判断Pull Request是否已经合并

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    apiInstance.getV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, accessToken);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsNumberMerge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="getV5ReposOwnerRepoPullsNumberOperateLogs"></a>
# **getV5ReposOwnerRepoPullsNumberOperateLogs**
> OperateLog getV5ReposOwnerRepoPullsNumberOperateLogs(owner, repo, number, accessToken, sort)

获取某个Pull Request的操作日志

获取某个Pull Request的操作日志

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String accessToken = "accessToken_example"; // String | 用户授权码
String sort = "desc"; // String | 按递增(asc)或递减(desc)排序，默认：递减
try {
    OperateLog result = apiInstance.getV5ReposOwnerRepoPullsNumberOperateLogs(owner, repo, number, accessToken, sort);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#getV5ReposOwnerRepoPullsNumberOperateLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **sort** | **String**| 按递增(asc)或递减(desc)排序，默认：递减 | [optional] [default to desc] [enum: desc, asc]

### Return type

[**OperateLog**](OperateLog.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoPullsCommentsId"></a>
# **patchV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments patchV5ReposOwnerRepoPullsCommentsId(owner, repo, id, body, accessToken)

编辑评论

编辑评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer id = 56; // Integer | 评论的ID
String body = "body_example"; // String | 必填。评论内容
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequestComments result = apiInstance.patchV5ReposOwnerRepoPullsCommentsId(owner, repo, id, body, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#patchV5ReposOwnerRepoPullsCommentsId");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **id** | **Integer**| 评论的ID |
 **body** | **String**| 必填。评论内容 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="patchV5ReposOwnerRepoPullsNumber"></a>
# **patchV5ReposOwnerRepoPullsNumber**
> PullRequest patchV5ReposOwnerRepoPullsNumber(owner, repo, number, body)

更新Pull Request信息

更新Pull Request信息

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
PullRequestUpdateParam body = new PullRequestUpdateParam(); // PullRequestUpdateParam | 可选。Pull Request 内容
try {
    PullRequest result = apiInstance.patchV5ReposOwnerRepoPullsNumber(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#patchV5ReposOwnerRepoPullsNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**PullRequestUpdateParam**](PullRequestUpdateParam.md)| 可选。Pull Request 内容 |

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPulls"></a>
# **postV5ReposOwnerRepoPulls**
> PullRequest postV5ReposOwnerRepoPulls(owner, repo, title, head, base, accessToken, body, milestoneNumber, labels, issue, assignees, testers, pruneSourceBranch)

创建Pull Request

创建Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
String title = "title_example"; // String | 必填。Pull Request 标题
String head = "head_example"; // String | 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch
String base = "base_example"; // String | 必填。Pull Request 提交目标分支的名称
String accessToken = "accessToken_example"; // String | 用户授权码
String body = "body_example"; // String | 可选。Pull Request 内容
Integer milestoneNumber = 56; // Integer | 可选。里程碑序号(id)
String labels = "labels_example"; // String | 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
String issue = "issue_example"; // String | 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充
String assignees = "assignees_example"; // String | 可选。审查人员username，可多个，半角逗号分隔，如：(username1,username2)
String testers = "testers_example"; // String | 可选。测试人员username，可多个，半角逗号分隔，如：(username1,username2)
Boolean pruneSourceBranch = true; // Boolean | 可选。合并PR后是否删除源分支，默认false（不删除）
try {
    PullRequest result = apiInstance.postV5ReposOwnerRepoPulls(owner, repo, title, head, base, accessToken, body, milestoneNumber, labels, issue, assignees, testers, pruneSourceBranch);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postV5ReposOwnerRepoPulls");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **title** | **String**| 必填。Pull Request 标题 |
 **head** | **String**| 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch |
 **base** | **String**| 必填。Pull Request 提交目标分支的名称 |
 **accessToken** | **String**| 用户授权码 | [optional]
 **body** | **String**| 可选。Pull Request 内容 | [optional]
 **milestoneNumber** | **Integer**| 可选。里程碑序号(id) | [optional]
 **labels** | **String**| 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance | [optional]
 **issue** | **String**| 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充 | [optional]
 **assignees** | **String**| 可选。审查人员username，可多个，半角逗号分隔，如：(username1,username2) | [optional]
 **testers** | **String**| 可选。测试人员username，可多个，半角逗号分隔，如：(username1,username2) | [optional]
 **pruneSourceBranch** | **Boolean**| 可选。合并PR后是否删除源分支，默认false（不删除） | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPullsNumberAssignees"></a>
# **postV5ReposOwnerRepoPullsNumberAssignees**
> PullRequest postV5ReposOwnerRepoPullsNumberAssignees(owner, repo, number, assignees, accessToken)

指派用户审查 Pull Request

指派用户审查 Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String assignees = "assignees_example"; // String | 用户的个人空间地址, 以 , 分隔
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequest result = apiInstance.postV5ReposOwnerRepoPullsNumberAssignees(owner, repo, number, assignees, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postV5ReposOwnerRepoPullsNumberAssignees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **assignees** | **String**| 用户的个人空间地址, 以 , 分隔 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPullsNumberComments"></a>
# **postV5ReposOwnerRepoPullsNumberComments**
> PullRequestComments postV5ReposOwnerRepoPullsNumberComments(owner, repo, number, body)

提交Pull Request评论

提交Pull Request评论

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
PullRequestCommentPostParam body = new PullRequestCommentPostParam(); // PullRequestCommentPostParam | 评论内容
try {
    PullRequestComments result = apiInstance.postV5ReposOwnerRepoPullsNumberComments(owner, repo, number, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postV5ReposOwnerRepoPullsNumberComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**PullRequestCommentPostParam**](PullRequestCommentPostParam.md)| 评论内容 |

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="postV5ReposOwnerRepoPullsNumberTesters"></a>
# **postV5ReposOwnerRepoPullsNumberTesters**
> PullRequest postV5ReposOwnerRepoPullsNumberTesters(owner, repo, number, testers, accessToken)

指派用户测试 Pull Request

指派用户测试 Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
String testers = "testers_example"; // String | 用户的个人空间地址, 以 , 分隔
String accessToken = "accessToken_example"; // String | 用户授权码
try {
    PullRequest result = apiInstance.postV5ReposOwnerRepoPullsNumberTesters(owner, repo, number, testers, accessToken);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#postV5ReposOwnerRepoPullsNumberTesters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **testers** | **String**| 用户的个人空间地址, 以 , 分隔 |
 **accessToken** | **String**| 用户授权码 | [optional]

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

<a name="putV5ReposOwnerRepoPullsNumberMerge"></a>
# **putV5ReposOwnerRepoPullsNumberMerge**
> putV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, body)

合并Pull Request

合并Pull Request

### Example
```java
// Import classes:
//import com.gitee.ApiException;
//import com.gitee.api.PullRequestsApi;


PullRequestsApi apiInstance = new PullRequestsApi();
String owner = "owner_example"; // String | 仓库所属空间地址(企业、组织或个人的地址path)
String repo = "repo_example"; // String | 仓库路径(path)
Integer number = 56; // Integer | 第几个PR，即本仓库PR的序数
PullRequestMergePutParam body = new PullRequestMergePutParam(); // PullRequestMergePutParam | PullRequest合入参数
try {
    apiInstance.putV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, body);
} catch (ApiException e) {
    System.err.println("Exception when calling PullRequestsApi#putV5ReposOwnerRepoPullsNumberMerge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **String**| 仓库所属空间地址(企业、组织或个人的地址path) |
 **repo** | **String**| 仓库路径(path) |
 **number** | **Integer**| 第几个PR，即本仓库PR的序数 |
 **body** | [**PullRequestMergePutParam**](PullRequestMergePutParam.md)| PullRequest合入参数 |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, multipart/form-data
 - **Accept**: application/json

