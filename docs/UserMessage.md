
# UserMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**sender** | [**UserBasic**](UserBasic.md) | 发送者 |  [optional]
**unread** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**updatedAt** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]



