
# UserNotificationCount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCount** | **Integer** | 通知总数 |  [optional]
**notificationCount** | **Integer** | 通知数量 |  [optional]
**messageCount** | **Integer** | 私信数量 |  [optional]



