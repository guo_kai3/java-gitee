
# UserNotificationNamespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



