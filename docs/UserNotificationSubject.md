
# UserNotificationSubject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**latestCommentUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



