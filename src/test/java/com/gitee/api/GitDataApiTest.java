/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.api;

import com.gitee.ApiException;
import com.gitee.model.Blob;
import com.gitee.model.Tree;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for GitDataApi
 */
@Ignore
public class GitDataApiTest {

    private final GitDataApi api = new GitDataApi();

    
    /**
     * 获取文件Blob
     *
     * 获取文件Blob
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoGitBlobsShaTest() throws ApiException {
        String owner = null;
        String repo = null;
        String sha = null;
        String accessToken = null;
        Blob response = api.getV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取目录Tree
     *
     * 获取目录Tree
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoGitTreesShaTest() throws ApiException {
        String owner = null;
        String repo = null;
        String sha = null;
        String accessToken = null;
        Integer recursive = null;
        Tree response = api.getV5ReposOwnerRepoGitTreesSha(owner, repo, sha, accessToken, recursive);

        // TODO: test validations
    }
    
}
