/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.api;

import com.gitee.ApiException;
import com.gitee.model.Issue;
import com.gitee.model.IssueCommentPostParam;
import com.gitee.model.IssueUpdateParam;
import com.gitee.model.Label;
import com.gitee.model.Note;
import com.gitee.model.OperateLog;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for IssuesApi
 */
@Ignore
public class IssuesApiTest {

    private final IssuesApi api = new IssuesApi();

    
    /**
     * 删除Issue某条评论
     *
     * 删除Issue某条评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteV5ReposOwnerRepoIssuesCommentsIdTest() throws ApiException {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        api.deleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取某个企业的所有Issues
     *
     * 获取某个企业的所有Issues
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5EnterprisesEnterpriseIssuesTest() throws ApiException {
        String enterprise = null;
        String accessToken = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        String schedule = null;
        String deadline = null;
        String createdAt = null;
        String finishedAt = null;
        String milestone = null;
        String assignee = null;
        String creator = null;
        String program = null;
        List<Issue> response = api.getV5EnterprisesEnterpriseIssues(enterprise, accessToken, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program);

        // TODO: test validations
    }
    
    /**
     * 获取企业的某个Issue
     *
     * 获取企业的某个Issue
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5EnterprisesEnterpriseIssuesNumberTest() throws ApiException {
        String enterprise = null;
        String number = null;
        String accessToken = null;
        Issue response = api.getV5EnterprisesEnterpriseIssuesNumber(enterprise, number, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取企业某个Issue所有评论
     *
     * 获取企业某个Issue所有评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5EnterprisesEnterpriseIssuesNumberCommentsTest() throws ApiException {
        String enterprise = null;
        String number = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<Note> response = api.getV5EnterprisesEnterpriseIssuesNumberComments(enterprise, number, accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 获取企业某个Issue所有标签
     *
     * 获取企业某个Issue所有标签
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5EnterprisesEnterpriseIssuesNumberLabelsTest() throws ApiException {
        String enterprise = null;
        String number = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<Label> response = api.getV5EnterprisesEnterpriseIssuesNumberLabels(enterprise, number, accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 获取当前授权用户的所有Issues
     *
     * 获取当前授权用户的所有Issues
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5IssuesTest() throws ApiException {
        String accessToken = null;
        String filter = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        String schedule = null;
        String deadline = null;
        String createdAt = null;
        String finishedAt = null;
        List<Issue> response = api.getV5Issues(accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);

        // TODO: test validations
    }
    
    /**
     * 获取当前用户某个组织的Issues
     *
     * 获取当前用户某个组织的Issues
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5OrgsOrgIssuesTest() throws ApiException {
        String org = null;
        String accessToken = null;
        String filter = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        String schedule = null;
        String deadline = null;
        String createdAt = null;
        String finishedAt = null;
        List<Issue> response = api.getV5OrgsOrgIssues(org, accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);

        // TODO: test validations
    }
    
    /**
     * 获取某个Issue下的操作日志
     *
     * 获取某个Issue下的操作日志
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerIssuesNumberOperateLogsTest() throws ApiException {
        String owner = null;
        String number = null;
        String accessToken = null;
        String repo = null;
        String sort = null;
        List<OperateLog> response = api.getV5ReposOwnerIssuesNumberOperateLogs(owner, number, accessToken, repo, sort);

        // TODO: test validations
    }
    
    /**
     * 仓库的所有Issues
     *
     * 仓库的所有Issues
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoIssuesTest() throws ApiException {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        String schedule = null;
        String deadline = null;
        String createdAt = null;
        String finishedAt = null;
        String milestone = null;
        String assignee = null;
        String creator = null;
        String program = null;
        List<Issue> response = api.getV5ReposOwnerRepoIssues(owner, repo, accessToken, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt, milestone, assignee, creator, program);

        // TODO: test validations
    }
    
    /**
     * 获取仓库所有Issue的评论
     *
     * 获取仓库所有Issue的评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoIssuesCommentsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String accessToken = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        Note response = api.getV5ReposOwnerRepoIssuesComments(owner, repo, accessToken, sort, direction, since, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 获取仓库Issue某条评论
     *
     * 获取仓库Issue某条评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoIssuesCommentsIdTest() throws ApiException {
        String owner = null;
        String repo = null;
        Integer id = null;
        String accessToken = null;
        Note response = api.getV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, accessToken);

        // TODO: test validations
    }
    
    /**
     * 仓库的某个Issue
     *
     * 仓库的某个Issue
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoIssuesNumberTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        Issue response = api.getV5ReposOwnerRepoIssuesNumber(owner, repo, number, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取仓库某个Issue所有的评论
     *
     * 获取仓库某个Issue所有的评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5ReposOwnerRepoIssuesNumberCommentsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        String accessToken = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        Note response = api.getV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, accessToken, since, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 获取授权用户的所有Issues
     *
     * 获取授权用户的所有Issues
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserIssuesTest() throws ApiException {
        String accessToken = null;
        String filter = null;
        String state = null;
        String labels = null;
        String sort = null;
        String direction = null;
        String since = null;
        Integer page = null;
        Integer perPage = null;
        String schedule = null;
        String deadline = null;
        String createdAt = null;
        String finishedAt = null;
        List<Issue> response = api.getV5UserIssues(accessToken, filter, state, labels, sort, direction, since, page, perPage, schedule, deadline, createdAt, finishedAt);

        // TODO: test validations
    }
    
    /**
     * 更新Issue
     *
     * 更新Issue
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void patchV5ReposOwnerIssuesNumberTest() throws ApiException {
        String owner = null;
        String number = null;
        IssueUpdateParam body = null;
        Issue response = api.patchV5ReposOwnerIssuesNumber(owner, number, body);

        // TODO: test validations
    }
    
    /**
     * 更新Issue某条评论
     *
     * 更新Issue某条评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void patchV5ReposOwnerRepoIssuesCommentsIdTest() throws ApiException {
        String owner = null;
        String repo = null;
        Integer id = null;
        String body = null;
        String accessToken = null;
        Note response = api.patchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, accessToken);

        // TODO: test validations
    }
    
    /**
     * 创建Issue
     *
     * 创建Issue
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void postV5ReposOwnerIssuesTest() throws ApiException {
        String owner = null;
        String title = null;
        String accessToken = null;
        String repo = null;
        String issueType = null;
        String body = null;
        String assignee = null;
        Integer milestone = null;
        String labels = null;
        String program = null;
        Issue response = api.postV5ReposOwnerIssues(owner, title, accessToken, repo, issueType, body, assignee, milestone, labels, program);

        // TODO: test validations
    }
    
    /**
     * 创建某个Issue评论
     *
     * 创建某个Issue评论
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void postV5ReposOwnerRepoIssuesNumberCommentsTest() throws ApiException {
        String owner = null;
        String repo = null;
        String number = null;
        IssueCommentPostParam body = null;
        Note response = api.postV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body);

        // TODO: test validations
    }
    
}
