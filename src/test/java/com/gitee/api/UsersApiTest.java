/*
 * 码云 Open API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.3.2
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.gitee.api;

import com.gitee.ApiException;
import com.gitee.model.Namespace;
import com.gitee.model.SSHKey;
import com.gitee.model.SSHKeyBasic;
import com.gitee.model.User;
import com.gitee.model.UserBasic;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for UsersApi
 */
@Ignore
public class UsersApiTest {

    private final UsersApi api = new UsersApi();

    
    /**
     * 取消关注一个用户
     *
     * 取消关注一个用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteV5UserFollowingUsernameTest() throws ApiException {
        String username = null;
        String accessToken = null;
        api.deleteV5UserFollowingUsername(username, accessToken);

        // TODO: test validations
    }
    
    /**
     * 删除一个公钥
     *
     * 删除一个公钥
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void deleteV5UserKeysIdTest() throws ApiException {
        Integer id = null;
        String accessToken = null;
        api.deleteV5UserKeysId(id, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取授权用户的资料
     *
     * 获取授权用户的资料
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserTest() throws ApiException {
        String accessToken = null;
        User response = api.getV5User(accessToken);

        // TODO: test validations
    }
    
    /**
     * 列出授权用户的关注者
     *
     * 列出授权用户的关注者
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserFollowersTest() throws ApiException {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<UserBasic> response = api.getV5UserFollowers(accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 列出授权用户正关注的用户
     *
     * 列出授权用户正关注的用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserFollowingTest() throws ApiException {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<UserBasic> response = api.getV5UserFollowing(accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 检查授权用户是否关注了一个用户
     *
     * 检查授权用户是否关注了一个用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserFollowingUsernameTest() throws ApiException {
        String username = null;
        String accessToken = null;
        api.getV5UserFollowingUsername(username, accessToken);

        // TODO: test validations
    }
    
    /**
     * 列出授权用户的所有公钥
     *
     * 列出授权用户的所有公钥
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserKeysTest() throws ApiException {
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<SSHKey> response = api.getV5UserKeys(accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 获取一个公钥
     *
     * 获取一个公钥
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserKeysIdTest() throws ApiException {
        Integer id = null;
        String accessToken = null;
        SSHKey response = api.getV5UserKeysId(id, accessToken);

        // TODO: test validations
    }
    
    /**
     * 获取授权用户的一个 Namespace
     *
     * 获取授权用户的一个 Namespace
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserNamespaceTest() throws ApiException {
        String path = null;
        String accessToken = null;
        List<Namespace> response = api.getV5UserNamespace(path, accessToken);

        // TODO: test validations
    }
    
    /**
     * 列出授权用户所有的 Namespace
     *
     * 列出授权用户所有的 Namespace
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UserNamespacesTest() throws ApiException {
        String accessToken = null;
        String mode = null;
        List<Namespace> response = api.getV5UserNamespaces(accessToken, mode);

        // TODO: test validations
    }
    
    /**
     * 获取一个用户
     *
     * 获取一个用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UsersUsernameTest() throws ApiException {
        String username = null;
        String accessToken = null;
        User response = api.getV5UsersUsername(username, accessToken);

        // TODO: test validations
    }
    
    /**
     * 列出指定用户的关注者
     *
     * 列出指定用户的关注者
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UsersUsernameFollowersTest() throws ApiException {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<UserBasic> response = api.getV5UsersUsernameFollowers(username, accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 列出指定用户正在关注的用户
     *
     * 列出指定用户正在关注的用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UsersUsernameFollowingTest() throws ApiException {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<UserBasic> response = api.getV5UsersUsernameFollowing(username, accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 检查指定用户是否关注目标用户
     *
     * 检查指定用户是否关注目标用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UsersUsernameFollowingTargetUserTest() throws ApiException {
        String username = null;
        String targetUser = null;
        String accessToken = null;
        api.getV5UsersUsernameFollowingTargetUser(username, targetUser, accessToken);

        // TODO: test validations
    }
    
    /**
     * 列出指定用户的所有公钥
     *
     * 列出指定用户的所有公钥
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getV5UsersUsernameKeysTest() throws ApiException {
        String username = null;
        String accessToken = null;
        Integer page = null;
        Integer perPage = null;
        List<SSHKeyBasic> response = api.getV5UsersUsernameKeys(username, accessToken, page, perPage);

        // TODO: test validations
    }
    
    /**
     * 更新授权用户的资料
     *
     * 更新授权用户的资料
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void patchV5UserTest() throws ApiException {
        String accessToken = null;
        String name = null;
        String blog = null;
        String weibo = null;
        String bio = null;
        User response = api.patchV5User(accessToken, name, blog, weibo, bio);

        // TODO: test validations
    }
    
    /**
     * 添加一个公钥
     *
     * 添加一个公钥
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void postV5UserKeysTest() throws ApiException {
        String key = null;
        String title = null;
        String accessToken = null;
        SSHKey response = api.postV5UserKeys(key, title, accessToken);

        // TODO: test validations
    }
    
    /**
     * 关注一个用户
     *
     * 关注一个用户
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void putV5UserFollowingUsernameTest() throws ApiException {
        String username = null;
        String accessToken = null;
        api.putV5UserFollowingUsername(username, accessToken);

        // TODO: test validations
    }
    
}
